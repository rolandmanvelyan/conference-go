from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state,
        "per_page": 1
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    print(response.status_code)
    print(response.json)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError):
        return {"picture_url": None}
    # Create a dictionary for the headers to use in the requestc
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    
def get_weather_data(city, state):

    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    
    geo_params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }

    response = requests.get(geo_url, params=geo_params)
    content = json.loads(response.content)
    
    try:
        lat = content[0]['lat'],
        lon = content[0]['lon']
    except (KeyError, IndexError):
        return None
    
    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }

    weather_data = requests.get(weather_url, params=weather_params)
    content = json.loads(weather_data.content) 

    try:
        return { 
        "temp": content["main"]["temp"],
        "description": content["weather"][0]["description"],
    }
    except (KeyError, IndexError):
        return None

    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response
    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary 

